$(document).ready(function(){

    $(".Nav__Mobile").click(function(){
      $(this).toggleClass("Nav__Mobile--open");
      $(".Nav__List").toggleClass("Nav__List--open");
    });

    var membercount = $(".Team__Member").length;
    var modulo = 5;

    var screen = $(window).width();
    if(screen > 1400) {
      modulo = 5;
    } else if (screen > 1200)  {
      modulo = 4;
    } else if(screen > 992) {
      modulo = 3;
    } else if(screen > 768)  {
      modulo = 2;
    } else {
      modulo = undefined;
    }
    
    $(".Team__Member").hover(function() {
      var id = $(this).attr('id');

      if(id % modulo == 0 || id == membercount) {
        var nextelement =  parseInt(id) - 1;
        $("#" + nextelement).addClass("Team__Member--info");
        $("#" + nextelement).append('<div class="Team__Text"><div class="Team__Name">' + $(this).data("name") + '</div><div class="Team__Comment">' + $(this).data("comment") +'</div></div>');
      } else {
        var nextelement =  parseInt(id) + 1;
        $("#" + nextelement).addClass("Team__Member--info");
        $("#" + nextelement).append('<div class="Team__Text"><div class="Team__Name">' + $(this).data("name") + '</div><div class="Team__Comment">' + $(this).data("comment") +'</div></div>');
      }

    }, function() {
      var idTwo = $(this).attr('id');
      if(idTwo % modulo == 0 || idTwo == membercount) {
        var nextelementTwo =  parseInt(idTwo) - 1;
        $("#" + nextelementTwo).empty('');
        $("#" + nextelementTwo).removeClass('Team__Member--info');
      } else {
        var idTwo = $(this).attr('id');
        var nextelementTwo =  parseInt(idTwo) + 1;
        $("#" + nextelementTwo).empty('');
        $("#" + nextelementTwo).removeClass('Team__Member--info');
      }
    });

  });